// ==UserScript==
// @name          Rickroll BBC
// @require       http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js
// @match         http://www.bbc.co.uk/*
// ==/UserScript==

// Task: Replace all links on BBC page with rickrolls

// Hints:
// * `$("a")` finds all links on a page.
// * `$("some_selector").attr("some_attribute", "some_value")` changes value of an attribute for all matching elemnets.
// * `href` attribute determines where a link is pointing.


// Solution:

var all = document.getElementsByTagName("a");

for (var i=0, max=all.length; i < max; i++) {
     all[i].setAttribute("href", "https://www.kopamed.cf")
}
