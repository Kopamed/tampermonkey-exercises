// ==UserScript==
// @name       Make people's avatar photos on github larger.
// @require       http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js
// @match  https://github.com/*
// ==/UserScript==

// Task: Make people's avatar photos on github larger so they're easier to identify.

// Hints:
// * You can either set CSS properties `height` and `width` or HTML attributes `height` and `width`
// * `$("selector").css("key", "value")` sets CSS properties
// * `$("selector").attr("key", "value")` sets HTML attributes
// * `$(img[src*='gravatar'])` selects all images with `"gravatar"` in their URLs (that's where the images reside)


//Solution:

var avatars = document.getElementsByClassName("avatar");

function magnify(scale, obj) {
    let currWidth = obj.getAttribute("width");
    let currHeight = obj.getAttribute("height");
    obj.setAttribute("width", currWidth*scale);
    obj.setAttribute("height", currHeight*scale);
}

for (let i = 0; i < avatars.length; i++) {
    magnify(3, avatars[i]);
}
